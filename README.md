= CargoTube message parsing and validation

this project validates and parses the incomming messages according to 
the wamp-proto specification.

It is a separate project so it can be used in any other project that 
needs to validate wamp message.
