REBAR = rebar3
APP=sb_core

.PHONY: all ct test clean elvis compile

all: compile elvis eunit

clean:
	$(REBAR) cover -r 
	$(REBAR) clean

eunit:
	$(REBAR) eunit
	$(REBAR) cover -v

ct:
	$(REBAR) ct
	$(REBAR) cover -v

tests: elvis eunit ct dialyzer

elvis:
	$(REBAR) lint

dialyzer:
	$(REBAR) dialyzer 

compile:
	$(REBAR) compile
