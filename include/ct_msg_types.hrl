


-type ct_msg_type() :: hello | challenge | authenticate | welcome |
                         goodbye | abort | error | publish | published |
                         subscribe | subscribed | unsubscribe |
                         unsubscribed | event | call | cancel | result |
                         register | registered | unregister |
                         unregistered | invocation | interrupt | yield.

-type ct_msg_uri() :: binary().
-type ct_msg_id() :: non_neg_integer().
-type ct_msg_error_uri() :: binary() | atom().
-type ct_msg_authmetod() :: binary() | atom().

-type ct_msg_arguments() :: list() | undefined.
-type ct_msg_arugmentskw() :: map() | undefined.


-type ct_msg_hello() :: {hello, binary(), map()}.
-type ct_msg_challenge() :: {challenge, ct_msg_authmetod(), map()}.
-type ct_msg_authenticate() :: {authenticate, binary(), map()}.
-type ct_msg_welcome() :: {welcome, ct_msg_id(), map()}.
-type ct_msg_abort() :: {abort, map(), ct_msg_error_uri()}.
-type ct_msg_goodbye() :: {goodbye, map(), ct_msg_error_uri()}.
-type ct_msg_error() :: {error, ct_msg_type(), ct_msg_id(), map(),
                         ct_msg_error_uri(), ct_msg_arguments(),
                         ct_msg_arugmentskw()} .
-type ct_msg_publish() :: {publish, ct_msg_id(), map(), ct_msg_uri(),
                           ct_msg_arguments(), ct_msg_arugmentskw()}.
-type ct_msg_published() :: {published, ct_msg_id(), ct_msg_id()}.
-type ct_msg_subscribe() :: {subscribe, ct_msg_id(), map(), ct_msg_uri()}.
-type ct_msg_subscribed() :: {subscribed, ct_msg_id(), ct_msg_id()}.
-type ct_msg_unsubscribe() :: {unsubscribe, ct_msg_id(),
                               ct_msg_id()}.
-type ct_msg_unsubscribed() :: {unsubscribed, ct_msg_id()}.
-type ct_msg_event() ::{event, ct_msg_id(), ct_msg_id(), map(),
                        ct_msg_arguments(), ct_msg_arugmentskw()}.
-type ct_msg_call() :: {call, ct_msg_id(), map(), ct_msg_uri(),
                        ct_msg_arguments(), ct_msg_arugmentskw()}.
-type ct_msg_cancel() :: {cancel, ct_msg_id(), map()}.
-type ct_msg_result() :: {result, ct_msg_id(), map(),
                          ct_msg_arguments(), ct_msg_arugmentskw()}.
-type ct_msg_register() :: {register, ct_msg_id(), map(), ct_msg_uri()}.
-type ct_msg_registered() :: {registered, ct_msg_id(), ct_msg_id()}.
-type ct_msg_unregister() :: {unregister, ct_msg_id(), ct_msg_id()}.
-type ct_msg_unregistered() :: {unregistered, ct_msg_id()}.
-type ct_msg_invocation() ::{invocation, ct_msg_id(), ct_msg_id(),
                             map(), ct_msg_arguments(), ct_msg_arugmentskw()}.
-type ct_msg_interrupt() :: {interrupt, ct_msg_id(), map()}.
-type ct_msg_yield() :: {yield, ct_msg_id(), map(), ct_msg_arguments(),
                         ct_msg_arugmentskw()}.

-type ct_msg_ping() :: binary().
-type ct_msg_pong() :: binary().


-type ct_msg() :: ct_msg_hello() |
                  ct_msg_challenge() |
                  ct_msg_authenticate() |
                  ct_msg_welcome() |
                  ct_msg_abort() |
                  ct_msg_established().

-type ct_wamp_msg() :: [any()].

-type ct_msg_established() :: ct_msg_goodbye() |
                              ct_msg_error() |
                              ct_msg_publish() |
                              ct_msg_publish() |
                              ct_msg_published() |
                              ct_msg_subscribe() |
                              ct_msg_subscribed() |
                              ct_msg_unsubscribe() |
                              ct_msg_unsubscribed() |
                              ct_msg_event() |
                              ct_msg_call() |
                              ct_msg_cancel() |
                              ct_msg_result() |
                              ct_msg_register() |
                              ct_msg_registered() |
                              ct_msg_unregister() |
                              ct_msg_unregistered() |
                              ct_msg_invocation() |
                              ct_msg_interrupt() |
                              ct_msg_yield().

-type ct_msg_encoding_json() :: json.
-type ct_msg_encoding_raw_json() :: raw_json.
-type ct_msg_encoding_json_batched() :: json.
-type ct_msg_encoding_msgpack() :: msgpack.
-type ct_msg_encoding_raw_msgpack() :: raw_msgpack.
-type ct_msg_encoding_msgpack_batched() :: msgpack_batched.
-type ct_msg_encoding_raw_erlbin() :: raw_erlbin.
-type ct_msg_encoding_none() :: none.

-type ct_msg_encoding() :: ct_msg_encoding_json() |
                           ct_msg_encoding_raw_json() |
                           ct_msg_encoding_json_batched() |
                           ct_msg_encoding_msgpack() |
                           ct_msg_encoding_raw_msgpack() |
                           ct_msg_encoding_msgpack_batched() |
                           ct_msg_encoding_raw_erlbin() |
                           ct_msg_encoding_none() .
