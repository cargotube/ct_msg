%%
%% Copyright (c) 2014-2018 Bas Wegh
%%

-module(ct_msg).

-author("Bas Wegh").

-include("ct_msg.hrl").

-export([get_type/1, extract_session/1, get_request_id/1, get_uri/1, deserialize/2,
         serialize/2, value_to_internal/1, ping/1, pong/1]).

-define(JSONB_SEPARATOR, <<24>>).

-spec get_type(ct_msg()) -> ct_msg_type().
get_type(Msg) ->
    erlang:element(1, Msg).

-spec extract_session(MaybeWelcome) -> Result when MaybeWelcome :: ct_msg_welcome() |
                                                                   any(),
                                                   Result :: {ok, ct_msg_id()} |
                                                             {error, not_welcome}.
extract_session({welcome, SessionId, _}) ->
    {ok, SessionId};
extract_session(_) ->
    {error, not_welcome}.

-spec get_request_id(ct_msg()) -> {ok, ct_msg_id()} | {error, bad_message}.
get_request_id(Message) ->
    Type = get_type(Message),
    TypeIdPos =
        [{publish, 2},
         {published, 2},
         {subscribe, 2},
         {subscribed, 2},
         {unsubscribe, 2},
         {unsubscribed, 2},
         {call, 2},
         {invocation, 2},
         {yield, 2},
         {result, 2},
         {register, 2},
         {registered, 2},
         {unregister, 2},
         {unregistered, 2},
         {error, 3}],
    MaybeTypePos = lists:keyfind(Type, 1, TypeIdPos),
    maybe_get_request_id(MaybeTypePos, Message).

-spec get_uri(ct_msg()) -> {ok, ct_msg_uri()} | {error, bad_message}.
get_uri(Message) ->
    Type = get_type(Message),
    ValidMessageTypes = [publish, subscribe, call, register],
    IsValidType = lists:member(Type, ValidMessageTypes),
    maybe_get_uri(IsValidType, Message).

-spec deserialize(binary(), ct_msg_encoding()) -> {[ct_msg()], binary()}.
deserialize(Buffer, Encoding) ->
    ct_msg_serialization:deserialize(Buffer, Encoding).

-spec serialize(ct_msg(), ct_msg_encoding()) -> binary() | ct_wamp_msg().
serialize(Wamp, Enc) ->
    ct_msg_serialization:serialize(Wamp, Enc).

-spec value_to_internal(Value) -> map() | list() | number() | binary() when Value ::
                                                                                map() |
                                                                                list() |
                                                                                number() |
                                                                                binary() |
                                                                                atom().
value_to_internal(Value) ->
    ct_msg_conversion:value_to_internal(Value).

-spec ping(binary()) -> ct_msg_ping().
ping(Payload) ->
    ct_msg_serialization:ping(Payload).

-spec pong(binary()) -> ct_msg_pong().
pong(Payload) ->
    ct_msg_serialization:pong(Payload).

-spec maybe_get_request_id(Valid, Msg) -> Result when Valid :: false |
                                                               {atom(), non_neg_integer()},
                                                      Msg :: ct_msg(),
                                                      Result :: {ok, ct_msg_id()} |
                                                                {error, bad_message}.
maybe_get_request_id({_, Pos}, Message) ->
    {ok, erlang:element(Pos, Message)};
maybe_get_request_id(_, _) ->
    {error, bad_message}.

-spec maybe_get_uri(Valid, Msg) -> Result when Valid :: boolean(),
                                               Msg :: ct_msg(),
                                               Result :: {ok, ct_msg_uri()} | {error, bad_message}.
maybe_get_uri(true, Message) ->
    {ok, erlang:element(4, Message)};
maybe_get_uri(_, _) ->
    {error, bad_message}.
