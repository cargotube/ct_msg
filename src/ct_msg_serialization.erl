-module(ct_msg_serialization).

-include("ct_msg_types.hrl").

-export([serialize/2, deserialize/2, ping/1, pong/1]).

-define(JSONB_SEPARATOR, <<24>>).

-spec ping(binary()) -> binary().
ping(Payload) ->
    add_binary_frame(1, Payload).

-spec pong(binary()) -> binary().
pong(Payload) ->
    add_binary_frame(2, Payload).

-spec serialize(Message, Encoding) -> EncodedMessage when Message :: ct_msg(),
                                                          Encoding :: ct_msg_encoding(),
                                                          EncodedMessage :: binary() |
                                                                            ct_wamp_msg().
serialize(Message, Encoding) ->
    WampMsg = ct_msg_conversion:to_wamp(Message),
    serialize_message(WampMsg, Encoding).

-spec deserialize(Buffer, Encoding) -> {Messages, NewBuffer} when Buffer :: binary(),
                                                                  Encoding :: ct_msg_encoding(),
                                                                  Messages :: [ct_msg()],
                                                                  NewBuffer :: binary().
deserialize(Buffer, none) ->
    Buffer;
deserialize(Buffer, Enc)
    when Enc == raw_erlbin; Enc == raw_msgpack; Enc == msgpack_batched; Enc == raw_json ->
    deserialize_binary(Buffer, [], Enc);
deserialize(Buffer, msgpack) ->
    deserialize_msgpack(Buffer, []);
deserialize(Buffer, json) ->
    deserialize_json(Buffer);
deserialize(Buffer, json_batched) ->
    deserialize_json_batched(Buffer).

-spec serialize_message(Message, Encoding) -> EncodedMessage when Message ::
                                                                      ct_wamp_msg(),
                                                                  Encoding :: ct_msg_encoding(),
                                                                  EncodedMessage :: binary() |
                                                                                    ct_wamp_msg().
serialize_message(Msg, none) ->
    Msg;
serialize_message(Msg, msgpack) ->
    msgpack:pack(Msg, [{pack_str, from_binary}]);
serialize_message(Msg, msgpack_batched) ->
    serialize_message(Msg, raw_msgpack);
serialize_message(Msg, json) ->
    jsone:encode(Msg);
serialize_message(Msg, json_batched) ->
    Enc = jsone:encode(Msg),
    <<Enc/binary, ?JSONB_SEPARATOR/binary>>;
serialize_message(Message, raw_erlbin) ->
    Enc = term_to_binary(Message),
    add_binary_frame(Enc);
serialize_message(Message, raw_msgpack) ->
    Enc = msgpack:pack(Message, [{pack_str, from_binary}]),
    add_binary_frame(Enc);
serialize_message(Message, raw_json) ->
    Enc = jsone:encode(Message),
    add_binary_frame(Enc).

%% @private
-spec add_binary_frame(binary()) -> binary().
add_binary_frame(EncodedData) ->
    add_binary_frame(0, EncodedData).

-spec add_binary_frame(0 | 1 | 2, binary()) -> binary().
add_binary_frame(Type, EncodedData) ->
    Len = byte_size(EncodedData),
    <<Type:8, Len:24/unsigned-integer-big, EncodedData/binary>>.

-spec deserialize_json(binary()) -> {[ct_msg()], binary()}.
deserialize_json(Buffer) ->
    handle_json_result(jsone:try_decode(Buffer, []), Buffer).

-spec handle_json_result({ok, ct_msg(), binary()} | any(), binary()) ->
                            {[ct_msg()], binary()}.
handle_json_result({ok, Msg, NewBuffer}, _Buffer) ->
    {[ct_msg_conversion:to_internal(Msg)], NewBuffer};
handle_json_result(_, Buffer) ->
    {[], Buffer}.

-spec deserialize_json_batched(binary()) -> {[ct_msg()], binary()}.
deserialize_json_batched(Buffer) ->
    Wamps = binary:split(Buffer, [?JSONB_SEPARATOR], [global, trim]),
    Dec =
        fun (M, List) ->
                Msg = ct_msg_conversion:to_internal(jsone:decode(M, [])),
                [Msg | List]
        end,
    {lists:reverse(lists:foldl(Dec, [], Wamps)), <<"">>}.

-spec deserialize_msgpack(binary(), [ct_msg()]) -> {[ct_msg()], binary()}.
deserialize_msgpack(Buffer, Messages) ->
    handle_msgpack_result(msgpack:unpack_stream(Buffer, [{unpack_str, as_binary}]),
                          Messages,
                          Buffer).

-spec handle_msgpack_result(Result, Messages, Buffer) -> {Messages, Buffer} when Result ::
                                                                                     {error,
                                                                                      Reason ::
                                                                                          any()} |
                                                                                     {binary(),
                                                                                      binary()},
                                                                                 Messages ::
                                                                                     [ct_msg()],
                                                                                 Buffer :: binary().
handle_msgpack_result({error, incomplete}, Messages, Buffer) ->
    {lists:reverse(Messages), Buffer};
handle_msgpack_result({error, Reason}, _, _) ->
    error(Reason);
handle_msgpack_result({Msg0, NewBuffer}, Messages, _Buffer) ->
    Msg = ct_msg_conversion:to_internal(Msg0),
    deserialize_msgpack(NewBuffer, [Msg | Messages]).

-spec deserialize_binary(Buffer, Messages, Enc) -> {Messages, NewBuffer} when Buffer ::
                                                                                  binary(),
                                                                              Messages ::
                                                                                  [ct_msg()],
                                                                              Enc ::
                                                                                  ct_msg_encoding(),
                                                                              NewBuffer :: binary().
deserialize_binary(Buffer, Messages, Enc) ->
    {Msg0, NewBuffer} = extract_bin_msg(Buffer),
    deserialize_binary_msg(Msg0, Messages, Enc, NewBuffer).

-spec extract_bin_msg(Buffer) -> {MaybeMsg, NewBuffer} when Buffer :: binary(),
                                                            MaybeMsg :: none | {MsgType, binary()},
                                                            MsgType :: msg | ping | pong,
                                                            NewBuffer :: binary().
extract_bin_msg(<<LenType:32/unsigned-integer-big, Data/binary>> = Buffer) ->
    <<Type:8, Len:24>> = <<LenType:32>>,
    maybe_return_msg(Type, Len, Data, Buffer);
extract_bin_msg(Buffer) ->
    {none, Buffer}.

-spec maybe_return_msg(Type, Len, Data, Buffer) -> {MaybeMsg, NewBuffer} when Type ::
                                                                                  non_neg_integer(),
                                                                              Len ::
                                                                                  non_neg_integer(),
                                                                              Data :: binary(),
                                                                              Buffer :: binary(),
                                                                              MaybeMsg :: none |
                                                                                          {MsgType,
                                                                                           binary()},
                                                                              MsgType :: msg |
                                                                                         ping |
                                                                                         pong,
                                                                              NewBuffer :: binary().
maybe_return_msg(Type, Len, Data, _Buffer) when is_integer(Len), byte_size(Data) >= Len ->
    <<Payload:Len/binary, NewBuffer/binary>> = Data,
    convert_binary_type(Type, Payload, NewBuffer);
maybe_return_msg(_, _, _, Buffer) ->
    {none, Buffer}.

-spec convert_binary_type(integer(), binary(), binary()) ->
                             {{MsgType, binary()}, binary()} when MsgType :: msg | ping | pong.
convert_binary_type(0, Payload, NewBuffer) ->
    {{msg, Payload}, NewBuffer};
convert_binary_type(1, Payload, NewBuffer) ->
    {{ping, Payload}, NewBuffer};
convert_binary_type(2, Payload, NewBuffer) ->
    {{pong, Payload}, NewBuffer}.

-spec deserialize_binary_msg(BinMessage, Messages, Encoding, Buffer) ->
                                {Messages, Buffer} when BinMessage :: none | {MsgType, binary()},
                                                        MsgType :: msg | ping | pong,
                                                        Messages :: [ct_msg()],
                                                        Encoding :: ct_msg_encoding(),
                                                        Buffer :: binary().
deserialize_binary_msg(none, Messages, _, NewBuffer) ->
    {lists:reverse(Messages), NewBuffer};
deserialize_binary_msg({msg, Payload}, Messages, Enc, NewBuffer) ->
    Msg = to_internal_msg(deserialize_binary_msg(Payload, Enc)),
    deserialize_binary(NewBuffer, [Msg | Messages], Enc);
deserialize_binary_msg({ping, _} = Ping, Messages, Enc, NewBuffer) ->
    deserialize_binary(NewBuffer, [Ping | Messages], Enc);
deserialize_binary_msg({pong, _} = Pong, Messages, Enc, NewBuffer) ->
    deserialize_binary(NewBuffer, [Pong | Messages], Enc).

-spec deserialize_binary_msg(binary(), ct_msg_encoding()) -> {ok, ct_wamp_msg()} | any().
deserialize_binary_msg(Payload, raw_erlbin) ->
    deserialize_raw_erlbin(Payload);
deserialize_binary_msg(Payload, raw_msgpack) ->
    deserialize_raw_msgpack(Payload);
deserialize_binary_msg(Payload, msgpack_batched) ->
    deserialize_raw_msgpack(Payload);
deserialize_binary_msg(Payload, raw_json) ->
    deserialize_raw_json(Payload).

-spec deserialize_raw_msgpack(binary()) -> {ok, ct_wamp_msg()} | any().
deserialize_raw_msgpack(Data) ->
    msgpack:unpack(Data, [{unpack_str, as_binary}]).

-spec deserialize_raw_json(binary()) -> {ok, ct_wamp_msg()} | any().
deserialize_raw_json(Data) ->
    {ok, jsone:decode(Data, [])}.

-spec deserialize_raw_erlbin(binary()) -> {ok, ct_wamp_msg()} | any().
deserialize_raw_erlbin(Data) ->
    {ok, binary_to_term(Data)}.

-spec to_internal_msg(Input) -> Result when Input :: {ok, ct_wamp_msg()} | any(),
                                            Result :: ct_msg() | bad_message.
to_internal_msg({ok, Msg}) ->
    ct_msg_conversion:to_internal(Msg);
to_internal_msg(_) ->
    bad_message.
